


import java.util.Scanner;

/**
 *
 * @author DELL
 */
public class BookingMovie {
    public static void setLine(){
        System.out.println("");
        System.out.println("---------------------");
        System.out.println("");
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Welcome to CS Cinema");
                
        User u1 = new User();
        u1.inputUser();
        u1.showUser();
        setLine();


        Cinema c = new Cinema();
        c.chooseCinema();
        setLine();


        Movie m = new Movie();
        m.chooseMovie();
        setLine();

        
        m.chooseTime();
        setLine();
        
        Seat s = new Seat();
        s.chooseSeat();
        
    }
}