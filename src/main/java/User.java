
import java.util.Scanner;

public class User {
    private String name;
    private String tel;
    private String email;

    public void setUser(String name, String tel, String email) {
                this.name = name;
                this.tel = tel;
                this.email = email;
    }
    
    public void inputUser() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input Your Name: ");
        name  = sc.next();
        System.out.print("Please input Your Tel: ");
        tel = sc.next();
        System.out.print("Please input Your Email: ");
        email = sc.next();
        setUser(name,tel,email);
    }
   
    public String getName() {
                return name;
    }
    public String getEmail() {
                return email;
    }
    public String getTel() {
                return tel;
    }
    
    public void showUser() {
        System.out.println("Name: "+getName());
        System.out.println("Tel.: "+getTel());
        System.out.println("Email: "+getEmail());
        
    }
    
}